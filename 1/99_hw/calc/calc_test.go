package main

import (
	"testing"
	"bytes"
)

const (
	testOnlyDigital = `2 2 3
4
5
+
-
*
+
=`
	wantOnlyDigital   = "-10"
	testStackEmpty    = `33 33 - = = = =`
	testIncorrectData = `33 2 3 \n fa sd`
	testStackEmpty2   = `1 2 3 4 + + - - *`
	testStackEmpty3   = `1 2 3 4 + + - - +`
	testStackEmpty4   = `1 2 3 4 + + - - -`
)

func TestOnlyDigital(t *testing.T) {
	in := bytes.NewBufferString(testOnlyDigital)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err != nil {
		t.Errorf("OnlyDigital test failed: %s\n", err)
	}
	result := out.String()
	if result != wantOnlyDigital {
		t.Errorf("\nGot: %s\nWant: %s\n", result, wantOnlyDigital)
	}
}

func TestStackEmpty(t *testing.T) {
	in := bytes.NewBufferString(testStackEmpty)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err == nil {
		t.Errorf("StackEmpty test failed: %s\n", err)
	}
}

func TestIncorrectData(t *testing.T) {
	in := bytes.NewBufferString(testIncorrectData)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err == nil {
		t.Errorf("IncorrectData test failed: %s\n", err)
	}
}

func TestStackEmpty2(t *testing.T) {
	in := bytes.NewBufferString(testStackEmpty2)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err == nil {
		t.Errorf("StackEmpty2 test failed: %s\n", err)
	}
}

func TestStackEmpty3(t *testing.T) {
	in := bytes.NewBufferString(testStackEmpty3)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err == nil {
		t.Errorf("StackEmpty3 test failed: %s\n", err)
	}
}

func TestStackEmpty4(t *testing.T) {
	in := bytes.NewBufferString(testStackEmpty4)
	out := new(bytes.Buffer)
	stack := new(Stack)
	stack.New(20)

	err := calculateRPN(in, out, stack)
	if err == nil {
		t.Errorf("StackEmpty4 test failed: %s\n", err)
	}
}
