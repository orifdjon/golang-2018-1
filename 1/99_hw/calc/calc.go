package main

import (
	"fmt"
	/*"os"*/
	"bufio"
	"strconv"
	"strings"
	"io"
)


type Stack struct {
	data []int
}

func (this *Stack) New(initSize int) {
	this.data = make([]int, 0, initSize)
}

func (this *Stack) push(val int) {
	this.data = append(this.data, val)
}

func (this *Stack) pop() (int, error) {
	val := 0
	if len(this.data) > 0 {
		val = this.data[len(this.data)-1]
		this.data = this.data[:len(this.data)-1]
		return val, nil
	} else {
		return val, fmt.Errorf("stack is empty")
	}
}

func calculateRPN(reader io.Reader, out io.Writer, stack *Stack) error {
	in := bufio.NewScanner(reader)
	for in.Scan(){
		line := make([]string, 0, 10)
		str := in.Text()
		str = strings.Trim(str, "\n\r")
		line = append(line, str)
		if strings.Contains(str, " ") {
			line = strings.Split(str, " ")
		}
		for _, simbol := range line {
			switch simbol {
			case "":
				fallthrough
			case "\n":
				continue
			case "=":
				val, err := stack.pop()
				if err != nil {
					return fmt.Errorf("stack is empty")
				} else {
					fmt.Fprint(out, val)
				}
			case "+":
				val1, err1 := stack.pop()
				val2, err2 := stack.pop()
				if err1 != nil || err2 != nil {
					return fmt.Errorf("stack is empty")
				} else {
					stack.push(val1 + val2)
				}
			case "-":
				val1, err1 := stack.pop()
				val2, err2 := stack.pop()
				if err1 != nil || err2 != nil {
					return fmt.Errorf("stack is empty")
				} else {
					stack.push(val2 - val1)
				}
			case "*":
				val1, err1 := stack.pop()
				val2, err2 := stack.pop()
				if err1 != nil || err2 != nil {
					return fmt.Errorf("empty")
				} else {
					stack.push(val2 * val1)
				}
			default:
				if _, err := strconv.Atoi(simbol); err != nil {
					return fmt.Errorf("it's not integer\n")
				} else {
					val, _ := strconv.Atoi(string(simbol))
					stack.push(val)
				}
			}
		}
	}
	return nil
}

/*  Я закоментировал main, чтобы больше было покрытие
func main() {
	stack := new(Stack)
	stack.New(100)
	file, err := os.Open("golang-2018-1/1/99_hw/calc/only_digital.txt") // У меня на компе в GoLang файл понимает по такому пути
	if err != nil {
		fmt.Fprintln(os.Stderr, "can't open file")
	}
	err = calculateRPN(file, os.Stdout, stack)
}
*/
