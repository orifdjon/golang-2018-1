package main

import (
	"strconv"
	"sort"
	"fmt"
)

// сюда вам надо писать функции, которых не хватает, чтобы проходили тесты в gotchas_test.go
func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(slice []int) string {
	var str string //initialize str = ""
	for _, val := range slice {
		str += strconv.Itoa(val)
	}
	return str
}

func MergeSlices(fSlice []float32, iSlice []int32) []int {
	lenSlice := len(fSlice) + len(iSlice)
	slice := make([]int, 0, lenSlice)
	for i := 0; i < len(fSlice); i++ {
		slice = append(slice, int(fSlice[i]))
	}

	for i := 0; i < len(iSlice); i++ {
		slice = append(slice, int(iSlice[i]))
	}
	return slice
}

func GetMapValuesSortedByKey(hashTable map[int]string) []string {
	keys := make([]int, 0, len(hashTable))
	str := make([]string, len(hashTable))
	for key := range hashTable {
		keys = append(keys, key)
	}
	sort.Ints(keys)
	for i, sortKey := range keys {
		val, ok := hashTable[sortKey]
		if ok {
			fmt.Errorf("Don't have such as key in map ")
		}
		str[i] = val
	}
	return str
}
