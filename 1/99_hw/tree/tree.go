package main

import (
	"io"
	"os"
	"log"
	"sort"
	"path"
	"fmt"
)



func dirTree(writer io.Writer, path string, printFiles bool) error {
	if printFiles {
		err := readCurrentDirFile(path, "", writer)
		if err != nil {
			log.Fatalf("Failed opening directory: %s\n", err.Error())
		}
	} else {
		err := readCurrentDir(path, "", writer)
		if err != nil {
			log.Fatalf("Failed opening directory: %s\n", err.Error())
		}
	}
	return nil
}

func readCurrentDir(dirPath string, prefix string, writer io.Writer) error {
	dir, err := os.Open(dirPath)
	if err != nil {
		return err
	}
	defer dir.Close()

	dirName := dir.Name()

	names, _ := dir.Readdirnames(0)
	sort.Strings(names)

	countDirs := 0
	for _, name := range names {
		fullName := path.Join(dirName, name)
		fileInfo, _ := os.Stat(fullName)
		if fileInfo.IsDir() {
			countDirs++
		}
	}

	j := 0
	for _, name := range names {
		fullName := path.Join(dirName, name)
		fileInfo, _ := os.Stat(fullName)
		if !fileInfo.IsDir() {
			continue
		} else {
			j++
			if j == countDirs {
				fmt.Fprintf(writer, "%s%s\n", prefix+"└───", name)
				readCurrentDir(fullName, prefix+"\t", writer)
			} else {
				fmt.Fprintf(writer, "%s%s\n", prefix+"├───", name)
				readCurrentDir(fullName, prefix+"│\t", writer)
			}
		}
	}
	return nil
}

func readCurrentDirFile(dirPath string, prefix string, writer io.Writer) error {
	dir, err := os.Open(dirPath)
	if err != nil {
		return err
	}
	defer dir.Close()

	dirName := dir.Name()

	names, _ := dir.Readdirnames(0)
	sort.Strings(names)

	for i, name := range names {
		fullName := path.Join(dirName, name)
		finfo, _ := os.Stat(path.Join(dirName, name))

		fileSize := ""
		if !finfo.IsDir() {
			finfo, _ := os.Stat(path.Join(dirName, name))
			if finfo.Size() == 0 {
				fileSize = " (empty)"
			} else {
				fileSize = fmt.Sprintf(" (%db)", finfo.Size())
			}
		}

		if i == len(names)-1 {
			fmt.Fprintf(writer,"%s%s%s\n", prefix+"└───", name, fileSize)
			if finfo.IsDir() {
				readCurrentDirFile(fullName, prefix+"\t", writer)
			}
		} else {
			fmt.Fprintf(writer,"%s%s%s\n", prefix+"├───", name, fileSize)
			if finfo.IsDir() {
				readCurrentDirFile(fullName, prefix+"│\t", writer)
			}
		}
	}
	return nil
}
